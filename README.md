# Composes

#### Objetivo 

* Armazenar os composes dos meus projetos

#### Projetos Atuais

- [x] [Movie](https://gitlab.com/luizhs331/composes/-/tree/main/movie)

    #### Subprojetos 

    | Name | Link |
    | ------ | ------ |
    | MovieApi | https://gitlab.com/luizhs331/movieapi |
    | MovieListFrontEnd | https://gitlab.com/luizhs331/movielistfrontend |
    | CadastroProgramFrontEnd | https://gitlab.com/luizhs331/cadastroprogramfrontend |

    Em breve vou analisar uma maneira de baixar os projetos automaticamente, talvez algo como um .sh
    Poderia ser criada a imagem colocada no docker-hub, mas não sei se existe a necessidade por enquanto.

