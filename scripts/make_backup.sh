docker exec -it postgres_container /bin/bash
/usr/bin/pg_dump -U postgres movie_db | gzip -9 > /tmp/movie_db.sql.gz
exit
docker cp postgres_container:/tmp/movie_db.sql.gz $pwd/files

# dump .sql

/usr/bin/pg_dump -U postgres -d movie_db -f /tmp/movie_db_backup.sql