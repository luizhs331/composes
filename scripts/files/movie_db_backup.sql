--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: genre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genre (
    id_sequential integer NOT NULL,
    id_program integer NOT NULL,
    ds_genre character varying(25) NOT NULL,
    id_genre integer
);


ALTER TABLE public.genre OWNER TO postgres;

--
-- Name: genre_id_genre_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genre_id_genre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genre_id_genre_seq OWNER TO postgres;

--
-- Name: genre_id_genre_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genre_id_genre_seq OWNED BY public.genre.id_sequential;


--
-- Name: program; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.program (
    id_program integer NOT NULL,
    ds_title character varying(50) NOT NULL,
    dt_release date NOT NULL,
    ds_description character varying NOT NULL,
    tp_program smallint NOT NULL,
    ds_duration character varying(10),
    ds_backdrop character varying(100),
    ds_poster character varying(100),
    rating numeric(3,1),
    url_trailer character varying,
    ds_age_restriction character varying(2)
);


ALTER TABLE public.program OWNER TO postgres;

--
-- Name: program_id_program_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.program_id_program_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.program_id_program_seq OWNER TO postgres;

--
-- Name: program_id_program_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.program_id_program_seq OWNED BY public.program.id_program;


--
-- Name: review; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.review (
    id_review integer NOT NULL,
    id_program integer NOT NULL,
    ds_title character varying(200) NOT NULL,
    ds_author character varying(200) NOT NULL,
    dt_date date NOT NULL,
    ds_text character varying,
    nr_rating numeric(3,1)
);


ALTER TABLE public.review OWNER TO postgres;

--
-- Name: review_id_review_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.review_id_review_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.review_id_review_seq OWNER TO postgres;

--
-- Name: review_id_review_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.review_id_review_seq OWNED BY public.review.id_review;


--
-- Name: test; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.test (
    name integer
);


ALTER TABLE public.test OWNER TO postgres;

--
-- Name: genre id_sequential; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre ALTER COLUMN id_sequential SET DEFAULT nextval('public.genre_id_genre_seq'::regclass);


--
-- Name: program id_program; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program ALTER COLUMN id_program SET DEFAULT nextval('public.program_id_program_seq'::regclass);


--
-- Name: review id_review; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.review ALTER COLUMN id_review SET DEFAULT nextval('public.review_id_review_seq'::regclass);


--
-- Data for Name: genre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.genre (id_sequential, id_program, ds_genre, id_genre) FROM stdin;
9	3	Drama	1
13	4	Drama	1
16	5	Drama	1
26	32	Drama	1
29	33	Drama	1
32	34	Drama	1
35	35	Drama	1
42	36	Drama	1
46	37	Drama	1
49	38	Drama	1
54	39	Drama	1
58	40	Drama	1
62	41	Drama	1
66	42	Drama	1
70	43	Drama	1
74	44	Drama	1
82	46	Drama	1
87	47	Drama	1
92	49	Drama	1
103	52	Drama	1
109	53	Drama	1
112	54	Drama	1
115	55	Drama	1
6	3	Sci-Fi	2
10	4	Sci-Fi	2
36	35	Sci-Fi	2
59	40	Sci-Fi	2
63	41	Sci-Fi	2
67	42	Sci-Fi	2
83	46	Sci-Fi	2
95	50	Sci-Fi	2
104	52	Sci-Fi	2
7	3	Fantasy	3
11	4	Fantasy	3
37	35	Fantasy	3
60	40	Fantasy	3
64	41	Fantasy	3
68	42	Fantasy	3
75	44	Fantasy	3
79	45	Fantasy	3
84	46	Fantasy	3
96	50	Fantasy	3
99	51	Fantasy	3
105	52	Fantasy	3
116	55	Fantasy	3
38	35	Animação	4
43	36	Animação	4
47	37	Animação	4
50	38	Animação	4
55	39	Animação	4
61	40	Animação	4
65	41	Animação	4
69	42	Animação	4
71	43	Animação	4
76	44	Animação	4
80	45	Animação	4
85	46	Animação	4
88	47	Animação	4
93	49	Animação	4
97	50	Animação	4
100	51	Animação	4
106	52	Animação	4
110	53	Animação	4
113	54	Animação	4
8	3	Animacao	4
12	4	Animacao	4
27	32	Animacao	4
30	33	Animacao	4
33	34	Animacao	4
28	32	Romance	9
31	33	Romance	9
48	37	Romance	9
51	38	Romance	9
56	39	Romance	9
94	49	Romance	9
40	35	Aventura	12
44	36	Aventura	12
53	38	Aventura	12
72	43	Aventura	12
77	44	Aventura	12
101	51	Aventura	12
107	52	Aventura	12
117	55	Aventura	12
41	35	Ação	13
45	36	Ação	13
73	43	Ação	13
78	44	Ação	13
81	45	Ação	13
102	51	Ação	13
108	52	Ação	13
118	55	Ação	13
15	5	Thriller	6
114	54	Thriller	6
17	6	Mistério	7
98	50	Mistério	7
14	5	Crime	5
34	34	Comédia	11
39	35	Comédia	11
86	46	Comédia	11
111	53	Comédia	11
19	6	Ficção científica	10
52	38	Ficção científica	10
57	39	Ficção científica	10
18	6	suspense	8
119	56	Sci-Fi	2
120	57	Sci-Fi	2
121	58	Sci-Fi	2
122	66	Sci-Fi	2
123	66	Fantasy	3
124	66	Animação	4
125	66	Aventura	12
126	66	Ação	13
127	67	Sci-Fi	2
128	67	Fantasy	3
129	67	Animação	4
130	67	Aventura	12
131	67	Ação	13
132	68	Sci-Fi	2
133	68	Fantasy	3
134	68	Animação	4
135	68	Aventura	12
136	68	Ação	13
137	68	Drama	1
139	73	Sci-Fi	2
140	73	Fantasy	3
141	73	Animação	4
142	73	Aventura	12
143	74	Drama	1
144	74	Animação	4
145	74	Ação	13
146	75	Drama	1
147	75	Fantasy	3
148	75	Animação	4
149	76	Sci-Fi	2
150	76	Fantasy	3
151	76	Animação	4
152	76	Comédia	11
153	76	Aventura	12
154	76	Ação	13
155	77	Drama	1
156	77	Animação	4
157	77	Aventura	12
158	77	Ação	13
159	78	Drama	1
160	78	Sci-Fi	2
161	78	Fantasy	3
162	78	Animação	4
163	78	Mistério	7
164	78	Aventura	12
165	78	Ação	13
166	79	Drama	1
167	79	Animação	4
168	79	Romance	9
169	80	Drama	1
170	80	Fantasy	3
171	80	Crime	5
172	80	Mistério	7
173	81	Fantasy	3
174	81	Animação	4
175	81	Romance	9
176	81	Ficção científica	10
177	81	Aventura	12
178	81	Ação	13
179	82	Fantasy	3
180	82	Animação	4
181	82	Ação	13
182	83	Drama	1
183	83	Sci-Fi	2
184	83	Fantasy	3
185	83	Animação	4
186	84	Drama	1
187	84	Fantasy	3
188	84	Animação	4
189	84	Romance	9
190	85	Drama	1
191	85	Animação	4
192	85	Romance	9
193	86	Drama	1
194	86	Animação	4
195	86	Romance	9
196	87	Drama	1
197	87	Animação	4
\.


--
-- Data for Name: program; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.program (id_program, ds_title, dt_release, ds_description, tp_program, ds_duration, ds_backdrop, ds_poster, rating, url_trailer, ds_age_restriction) FROM stdin;
5	Drive	2011-09-16	Um motorista habilidoso é dublê em Hollywood e piloto de fuga em assaltos. Quando aceita ajudar o marido de sua vizinha Irene, fica na mira dos homens mais perigosos de LA. Mas o trabalho dá errado, e a única forma de garantir a segurança de Irene e do filho é fazer o que ele faz de melhor: dirigir.	1	1h 36m	drive-backdrop	drive-poster	7.8	\N	\N
35	Angel Beats	2010-06-26	Otonashi acorda e descobre que está morto. Uma menina chamada Yuri explica que eles estão na vida após a morte, e Otonashi percebe que a única coisa que ele se lembra de si mesmo é o seu nome. Yuri lhe conta que ela lidera a Shinda Sekai Sensen e guerreia contra uma garota chamada Tenshi. Incapaz de acreditar que Tenshi é maligna, Otonashi tenta falar com ela, mas o encontro não ocorre como imaginava. Otonashi decide juntar-se a SSS e lutar contra Tenshi, mas esta estranhamente atraído por ela. Ao tentar recuperar suas memórias e compreender Tenshi, ele gradualmente desvenda os mistérios da vida após a morte.	0	24m	5ebcadc4dbf9138ec034b7efa981b4c4	1f009e2375385e4847b6cec5f0803407	7.5	\N	\N
6	Erased	2016-06-01	Satoru Fujinuma, é devolvido 18 anos pelo tempo para evitar os eventos que levaram à morte de sua mãe, que começou com uma série de sequestros no seu quinto ano.	0	4h 40m	erased_backdrop	erased_poster	8.5	\N	16
34	Shigatsu wa Kimi no Uso	2014-10-10	Arima Kousei era um prodígio em se tratando de piano, dominou as competições infantis por anos e todos os músicos infantis conheciam seu nome. Mas com o falecimento da sua mãe, que também era sua instrutora, ele sofre um bloqueio mental no meio de um recital e nunca mais consegue ouvir o som do seu piano, mesmo com sua audição normal. Dois anos se passam e Kousei ainda não tocou em seu piano de novo e vive em um mundo sem cor e sem outros tons. Ele passa a aceitar sua condição e vive feliz com seus amigos, Tsubaki e Watari, até que um dia, uma garota muda tudo. Miyazono Kaori é uma linda violinista, e seu estilo reflete sua personalidade livre e animada. Ela ajuda Kousei a voltar ao mundo da música e mostra a ele que neste mundo ele pode ser livre e não seguir as regras, um jeito muito diferente da estrutura rígida e estilo regrado que Kousei estava acostumado.	0	23m	ce93533e3a25c3c799794c2663158838	d9f4bb68ed35af35d594c35635f1866e	8.5	\N	12
47	Koe no Katachi	2013-08-06	Nishimiya Shouko é uma estudante com deficiência auditiva. Durante o ensino fundamental, após se transferir para uma nova escola, Shouko passa a ser alvo de bullying e em pouco tempo precisa se transferir. O que ela não esperava é que alguns anos depois, Ishida Shouya, um dos valentões que tanto a fez sofrer no passado surgisse de novo em sua vida com um novo propósito.	1	2h 9m	5d41b5902201244be188cd0b15699590	401b40ce7bb59727fbcc4d4ade219b41	8.2	\N	\N
3	Evangelion: 1.11 Você (Não) Está Sozinho	2007-09-01	Tokyo-3 permanece em pé após a maior parte da civilização ter sido dizimada no Segundo Impacto. Agora, a cidade sofre o ataque incessante dos estranhos e mortais Anjos, criaturas bizarras empenhadas em exterminar a raça humana. Para combater esse cruel inimigo, a agência governamental NERV constrói uma frota de máquinas humanóides (os EVAs) e Shinji Ikari é chamado para entrar em ação! Vivendo uma vida de solidão e questionando sua própria existência, Shinji se esforça para aceitar a responsabilidade para a batalha pela sobrevivência da humanidade.	1	1h 38m	evangelion-backdrop	evangelion-poster	7.7	\N	\N
4	Evangelion 2.22: Você (não) Pode Avançar	2009-06-27	Sob constante ataque dos Anjos, a Nerv apresenta dois novos pilotos: Makinami Mari Illustrous e Asuka Langley Shikinami. Paralelamente à incursão, Gendo Ikari e Seele prosseguem o projeto secreto que envolve tanto Rei como Shinji.	1	1h 52m	evangelion-2.2-backdrop	evangelion-2.2-poster	8.0	\N	\N
36	Akame ga Kill	2013-09-25	Num mundo de fantasia, o guerreiro Tatsumi parte para a capital procurando uma maneira de ganhar dinheiro para ajudar sua vila, mas descobre um mundo de corrupção inimaginável, se espalhando através do Primeiro Ministro, que controla a orelha do Imperador criança. Depois de quase se tornar vítima dessa corrupção, Tatsumi é recrutado pela Night Raid, um grupo de assassinos dedicado a eliminar a corrupção que infesta a capital, matando impiedosamente os responsáveis.	0	24m	48497fd03175cf487560f6ee1c261625	5167c25308084dd4adc1fcfa4073d6ac	8.0	\N	16
49	Kimi no na wa	2016-08-26	Mitsuha é a filha do prefeito de uma pequena cidade, mas sonha em tentar a sorte em Tóquio. Taki trabalha em um restaurante em Tóquio e deseja largar o seu emprego. Os dois não se conhecem, mas estão conectados pelas imagens de seus sonhos.	1	1h 46	9ccca1ff095678298347355f38fd1f25	04afb817a4a31190c1daa23872b86ee2	8.5	\N	\N
50	Death Note	2006-08-01	O jovem estudante Light Yagami acha um caderno com poderes sobrenaturais, chamado Death Note, no qual é possível matar uma pessoa apenas escrevendo seu nome no caderno. Quando o descobre, Light tenta eliminar todos os criminosos do mundo e dar à sociedade um mundo livre do mal. Mas seus planos começam a sair de rumo quando o detetive L resolve contrariar Light.	0	22m	8b7f7d254caabd9509c69a8a03715fe1	27601af12f27a770b7710e35007efd18	8.5	\N	\N
51	Goblin Slayer	2018-10-07	Em um mundo de fantasia, um herói solitário ganha a vida exterminando cada duende que encontra. Mas um dia ele encontra um amigo e sua vida começa a ficar mais intensa.	0	30m	73801f0eb79522a977643e535ce6dac1	9ede6fb7ba949b1a3d67033b87f11b5a	7.5	\N	\N
52	Tate no Yuusha no Nariagari	2019-01-09	Iwatani Naofumi, um otaku como qualquer outro, encontra um livro numa biblioteca que o transporta para outro mundo. Ele recebe a missão de se tornar o Herói do Escudo, um dos Quatro Heróis Cardinais que enfrentará as Ondas de Catástrofe ao lado dos Heróis da Espada, Lança e Arco. Empolgado com as aventuras, Naofumi sai em missão com sua equipe. Contudo, alguns poucos dias depois, ele é traído e perde todo o seu dinheiro, dignidade e respeito. Será que ele vai encontrar uma saída dessa situação desesperadora?	0	24m	4dd7140bfc0605add16c573f56ee5527	1f30b4115b0dd28e991a0485f9bc5ce3	8.0	\N	\N
40	Violet Evergarden	2015-12-25	Num dado momento, no continente de Telesis. A grande guerra que dividiu o continente em Norte e Sul terminou após quatro anos, e as pessoas dão boas vindas a uma nova geração. Violet Evergarden, uma jovem conhecida como “a arma”, deixou o campo de batalha para começar uma nova vida no serviço de correios. Lá, ela fica profundamente emocionada pelo trabalho das “Autômatas de Automemórias”, que carregam os pensamentos das pessoas e as converte em palavras. Violet começa sua jornada como uma Autômata e fica frente a frente com as emoções de várias pessoas e diferentes formas de amor. Há algumas palavras que Violet ouviu no campo de batalha, que não se esqueceu. Essas palavras foram ditas por alguém que ela gosta, mais do que qualquer outra pessoa. Ela ainda não entende seu significado, mas procura encontrá-lo.	0	25m	3372654b640f5e3971bb4700a02955d7	0378ff6887467861a3e0eadc764c04b9	8.5	\N	\N
41	Neon Genesis Evangelion	1995-10-04	No ano 2000, um grande impacto de um meteoro na Antártida derreteu o continente gelado, elevando o nível dos mares. Chegou-se a conclusão que Deus estaria tentando eliminar a raça humana e previu-se o envio de "Angels" para acabar com a humanidade. Preparando-se para isso, criou-se uma grande organização, custeada por todo o mundo: A NERV. Seu objetivo seria o de proteger a humanidade, pesquisando formas de defesa. Assim nasceram os Evangelions (ou EVA's): Gigantescos robôs pilotados por alguém com o dom de "sincronizar" com o sistema.	0	25m	01f4d3c38d7f6e10d26ff0f89d815072	dec9a82ef2da280244dc3e9fdd512761	8.5	\N	\N
39	Voices of a Distant Star	2005-12-23	O projeto de exploração de Marte foi destruído por um ataque alienigena em 2039. Sete anos mais tarde, as Nações Unidas lançam uma missão de busca e destruição dos aliens no espaço profundo. Mikako e Noboru terminaram suas provas para entrarem na escola secundária... ou assim parecia ser. Entretanto Mikako é escolhida para participar como piloto desta missão, e isso os separa. Quando ela se encontra no espaço, sua única forma de comunicação é por e-mail, que demora cada vez mais para ir e vir, enquanto as naves espaciais viajam para fora do sistema solar.\n\n	1	25m	bbb4b838ee4698812cb4d3a68c387cca	ecaf2a378874f0a131f6eb41a452e1b0	7.0	\N	\N
38	The Place Promised in Our Early Days	2004-11-20	Três adolescentes decidem construir uma aeronave experimental capaz de atravessar a Hokkaido, na parte norte do país, e permitir que eles desvendem os segredos de uma misteriosa torre.	1	1h 30m	df94c9c2fb145bbd16612284646bb705	6d65e273c19d9b0ed7aea582e94af6bf	7.0	\N	\N
44	Saint Seiya: The Lost Canvas	2009-06-24	Os Cavaleiros do Zodíaco - The Lost Canvas é uma nova saga de "Os Cavaleiros do Zodíaco" que conta a antiga guerra santa contra Hades a 243 anos atrás, no século 18, na Europa. Nesta história não temos Seiya, Saori, Shun e todos os outros personagens que conhecemos. Os únicos conhecidos são Dohko de Libra, que era o Mestre Ancião que treinou Shiryu, e Shion de Áries, que era o Mestre do Santuário morto por Saga na época do anime clássico. Nessa nova saga, temos três personagens principais que foram criados desde a infância em um orfanato na Itália. Tenma, que é o Cavaleiro de Pégaso dessa época, Sasha que é a reencarnação de Atena e Alone, irmão de Sasha, que foi a pessoa escolhida para abrigar a alma de Hades.	0	24m	540e11db7ff4bb1ab485bc2868310458	b85bce2c0b4cc30578479c64432c9710	8.5	\N	\N
43	Vinland Saga	2019-07-07	Thorfinn é filho de um dos maiores guerreiros vikings, mas quando o seu pai é morto na batalha contra o mercenário Askeladd, ele jura vingança. Thorfinn se junta ao grupo de Askeladd para um dia desafiá-lo para um duelo e derrotá-lo, mas acaba se envolvendo na guerra pela coroa da Inglaterra.	0	25m	451e7c101cce33dcc02c2643b0273724	dd2aa8788e098ab2cdad9548df7b2565	9.0	\N	\N
45	Saint Seiya: Soul of Gold	2015-04-11	No Submundo, durante a luta contra Hades, os Cavaleiros de Ouro dão a própria vida para destruir o Muro das Lamentações e, assim, permitir que Seiya e seus amigos avancem na jornada. No entanto, Aioria e os demais cavaleiros de ouro, que deveriam ter sido aniquilados, acordam em um lugar belo e cheio de luz. Por que eles, que deveriam estar mortos, reviveram? Enquanto esse grande mistério permanece, em um novo encontro Aioria se envolve numa luta e então, quando a onda de Cosmo chega ao seu limite... acontece uma transformação na armadura de Leão!	0	24m	800caa17b8b5c3e809644a325cdaec48	035832129763fb41e6c6984b9e0a4241	8.0	\N	\N
37	I Want To Eat Your Pancreas	2018-09-01	Num mundo de fantasia, o guerreiro Tatsumi parte para a capital procurando uma maneira de ganhar dinheiro para ajudar sua vila, mas descobre um mundo de corrupção inimaginável, se espalhando através do Primeiro Ministro, que controla a orelha do Imperador criança. Depois de quase se tornar vítima dessa corrupção, Tatsumi é recrutado pela Night Raid, um grupo de assassinos dedicado a eliminar a corrupção que infesta a capital, matando impiedosamente os responsáveis.	1	1h 49m	42faa8128c1fc77557d24d71bd75d2da	e6e998a159fb4c27e0771a017088789a	8.0	https://www.youtube.com/embed/eUJrJPOKCHs	\N
42	Elfen Lied	2004-07-25	Diclonius, a nova evolução humana... mutantes, ditos, escolhidos por Deus para serem os aniquiladores da humanidade. Possuem dois chifres em suas cabeças e uma estranha habilidade similar a telecinese sob forma de braços. Extremamente perigosos e cientes de sua "missão", foram subjugados pelo governo que os mantém confinados em laboratórios. Lucy é uma Diclonios cuja jovem e bela aparência escondem uma personalidade psicótica e ela, após matar dezenas de guardas, escapa de seu confinamento, porém é alvejada por um tiro na cabeça e cai no mar... Ela é encontrada por Kouta e Yuka na praia, mas com uma personalidade totalmente diferente, infantil e inocente, sem memórias e a única coisa que fala é nyuu. Os dois levam-na para casa e decidem cuidar dela, batizando-a de Nyuu. Entretanto, parece que a Lucy ainda não está morta e nem se esqueceu de seu passado.	0	25m	3a5bbf4d4fac4139d6521115f234707b	347a657b48d69884b4702edf88f0cc07	8.0	\N	18
33	The Garden of Words	2013-05-31	Takao, que está treinando para ser sapateiro, matou aula e está desenhando sapatos em um jardim. Ele conhece uma misteriosa mulher, Yukino, que é mais velha do que ele. Então, sem marcar os horários, os dois começam a se ver periodicamente, mas somente em dias chuvosos. Eles aprofundam sua relação e se abrem um para o outro, mas o fim da temporada de chuva logo se aproxima...	1	46m	0432165a59dcee4db4fa1fb9681d1b14	bcaa1664fe3ac8fa2b98053209eaed5e	8.0	https://www.youtube.com/embed/udDIkl6z8X0	12
53	3D Kanojo: Real Girl	2018-04-04	Kanojo 3D conta a história de Hikari Tsutsui, um garoto do ensino médio que está satisfeito com as meninas virtuais que ele encontra em animes e jogos. Ele não tem muitos amigos e vive em seu próprio mundo. Um dia, quando ele estava limpando a piscina da escola, ele é abordado pela Iroha, uma “garota real” que é bem vista e popular entre os garotos da escola.	0	23m	db11ecbc3c0bc2a51697e47922010a4b	2c217ee0c758f6e69f7809d7d97d51a1	7.0	\N	\N
54	Perfect Blue	1997-07-25	Mima Kirigoe é membro de uma banda de música pop japonesa (J-Pop), chamada "CHAM!", que decide deixar a banda para se dedicar à carreira de atriz. Alguns fãs ficam descontentes com a repentina mudança de carreira, pois Mima, sendo um ídolo pop, é vista como uma menina inocente e angelical. Conforme avança em sua nova carreira, Mima mergulha em um intenso drama psicológico no qual fantasia e realidade se confundem colocando em dúvida sua ética moral.	1	1h 25m	19116e464fa0b4b935674b63b67b48f4	2c2926847ccb96c40c140c7cb0f9ec74	8.0	\N	\N
55	Hai To Gensou No Grimgar	2016-01-11	Medo, sobrevivência, instinto. Jogados em uma terra estrangeira com nada além de lembranças nebulosas e o conhecimento de seu nome, eles podem sentir apenas essas três emoções ressoando profundamente em suas almas. Um grupo de estranhos não tem outra escolha senão aceitar o único emprego remunerado neste mundo de jogo - o papel de um soldado no Exército da Reserva - e eliminar qualquer coisa que ameace a paz em seu novo mundo, Grimgar. > Quando todos os candidatos mais fortes se juntam, os que são deixados para trás devem criar um partido para sobreviver: Manato, líder e sacerdote carismático; Haruhiro, um ladrão nervoso; Yume, um caçador alegre; Shihoru, um mago tímido; Mogzo, um guerreiro gentil; e Ranta, um cavaleiro das trevas barulhento. Apesar de sua semelhança com uma, este não é um jogo - não há refazer ou reaparecer; é matar ou ser morto. Agora, cabe a esse grupo de lutadores improváveis ​​sobreviver juntos em um mundo onde a vida e a morte são separadas apenas por uma linha tênue.	0	24m	af7d904ee49e152129424a2b8e948b52	1bf4843539a539546d50b03fcfdef55f	7.5	\N	\N
32	5 centimeters per second	2007-03-03	Contada em três segmentos interligados, entre o início dos anos 1990 e 2007, um jovem chamado Takaki Tono e sua melhor amiga Akari Shinohara moram em Tóquio. Devido à mudanças no trabalho de seu pai, Akari acaba se transferindo de cidade com sua família, mas os dois lutam para manter um contato através de cartas. Seus desencontros são constantes e os dois acabam se afastando com o tempo, deixando apenas as memórias de momentos juntos. Novas pessoas surgem em suas vidas, mas Takaki não esquece de Akari, que mesmo com o tempo passado, tem a esperança de encontrá-la novamente.	1	1h 3m	08446c66566e8fe57c1a9a2d6b4284bd	6f55421d067f31d5c5346686bf816383	9.0	https://www.youtube.com/embed/wdM7athAem0	\N
46	Ano Natsu de Matteru	2012-03-01	Um grupo de amigos decide gravar um filme, enquanto estão de férias. Enquanto eles gravam o filme, eles vão aprendendo mais sobre o mundo do cinema e conhecendo melhor tanto os amigos que estão ali, quanto eles mesmos. O que começa como uma forma simples de evitar o mau humor do verão, rapidamente se transforma em algo muito mais complexo, intimidador e revelador. Mas, há algo misterioso por traz de tudo isso.	0	24m	a12a0e05115f5e604668fc8b3ad6a34d	c899aa239420c9c0cca4c3c0e895363f	8.0	https://www.youtube.com/embed/QHvKORoz6PQ	12
76	Chainsaw Man	2022-10-12	Denji tem um sonho simples - viver uma vida feliz e pacífica, passando um tempo com uma garota de quem gosta. Isso está muito longe da realidade, no entanto, já que Denji é forçado pela yakuza a matar demônios para pagar suas dívidas esmagadoras. Usando seu demônio de estimação Pochita como arma, ele está pronto para fazer qualquer coisa por um pouco de dinheiro.	0	24m	b0832b88cdb14c882adb6204806b62b9	d7d2a29df7dfb491f8dd645041eccffd	8.5	\N	\N
68	Attack on Titan	2013-04-07	Titãs estão quase exterminando a raça humana. Porém alguns estão dispostos a formar um exército de ataque aos seres assassinos. O jovem Eren, após ver sua mãe ser devorada por um titã, decide que não deixará nenhum deles vivo e buscará sua vingança completa.\n\n	0	25m	82c7035d59e44f9394f495f4b361e33f	01562e6e1140769a49a15a5057e7dfd2	9.0	https://www.youtube.com/embed/MGRm4IzK1SQ	16
77	Blue Lock	2022-10-08	Após sofrer um vexame na Copa do Mundo de 2018, a seleção japonesa sofre para se recompor. Falta ao time um artilheiro, uma estrela capaz de guiá-los à vitória. Com o intuito de criar um atacante com fome de bola e sede de vitória, capaz de virar jogos tidos como impossíveis, a Confederação Japonesa de Futebol reune 300 dos melhores jogadores de base do país em um só lugar. Centenas de jovens vão se enfrentar numa batalha de músculos e de egos para provar que são os melhores.	0	25m	684b2b49ccde8275150bc2d7f19280e6	3074f6d8e3cf27336da46229d27e9b59	8.5	\N	\N
73	Castlevania	2017-07-07	Um caçador de vampiros luta para salvar uma cidade sitiada por um exército de criaturas controladas pelo próprio Drácula. Inspirado no clássico videogame.	2	24m	2114e6e30355f3eb4af20a8e94d58571	bc158d500871162f3e829b823c0869ec	8.5	\N	16
74	Tokyo Ghoul	2014-07-04	Em Tóquio, criaturas conhecidas como ghouls vivem entre os humanos e os devoram para sobreviver. Dentre eles, o jovem universitário Ken Kaneki leva uma vida pacata entre livros, até que um trágico encontro o coloca diante desses seres e o obriga a lutar por sua humanidade.	0	24m	2fe93ec27014966b92bd573cb59cd9e7	2a2d3018b1d97b137ed810f3eddba80b	8.0	\N	\N
78	Deatte 5-byou de Battle	2021-07-13	Sem uma aparente explicação, pessoas de todo mundo começam a receber habilidades especiais depois de serem estranhamente raptadas. Em seguida, essas mesmas pessoas são colocadas em confrontos em uma espécie de experimento distorcido, onde ninguém sabe o que acontecem com aqueles que falharem. A história segue Akira Shiroyanagi, um garoto apaixonado por jogos que é colocado em batalha contra Mion, uma misteriosa garota que ele nunca viu antes.	0	24m	81f99c6e540701af29fa2f013983e884	8906af45e729c204e933ed85a32bb700	8.5	\N	\N
75	Bunny Girl Senpai	2018-10-04	 vida do estudante Sakuta Azusagawa tem uma reviravolta inesperada quando ele conhece a atriz adolescente Mai Sakurajima, vestida como uma coelhinha erótica e vagando por uma biblioteca sem ser notada por mais ninguém além de Sakuta.	0	24m	e39db9c00e7bad78003b214f233f0a79	e67a43ccc573b0e5cf4e5d0d58e1a663	8.0	\N	\N
79	Shiki Oriori	2018-08-04	Recordações evocadas por uma tigela de macarrão, a decadência de uma bela modelo e um primeiro amor com sabor agridoce -- três histórias urbanas ambientadas na China.	0	24m	7676ae11fc84136d628d014927d815be	d6ad2446faa46d7269ae18f5665a7a37	6.5	\N	\N
80	Dark	2017-12-01	Saga familiar sobrenatural ambientada em uma cidade alemã, onde o desaparecimento de duas crianças expõe as relações entre quatro famílias.	2	54m	2ab5b22372eb16f75b269075472b2b78	a73ef9939fbacec76ed0dea7b55e46af	9.0	\N	\N
81	Sword Art Online	2012-07-07	Em 2026, 4 anos após o infame incidente de Sword Art Online, uma nova forma de tecnologia emergiu: o Augma, um dispositivo que utiliza um sistema de Realidade Aumentada. Diferente da Realidade Virtual do NerveGear e do Amusphere, é perfeitamente seguro e permite aos jogadores à usarem enquanto estão conscientes, gerando um grande hit no mercado. A aplicação mais popular do Augma é o jogo Ordinal Scale, que emerge os jogadores em um RPG de fantasia com rankings e recompensas. Seguindo a nova mania, os amigos de Kirito mergulham no jogo e, apesar de seus receios sobre o sistema, Kirito eventualmente se junta à eles. Enquanto parece ser algo apenas para diversão e jogos à princípio, eles logo descobrem que o jogo não é tudo o que parece...	0	24m	c4a37e933697ca4f11cc80bb34a1aa9d	7a763b836baa0f1de6869977c35c404d	7.5	\N	\N
82	Demon Slayer: Kimetsu no Yaiba	2019-03-29	Tanjiro encontra sua família massacrada e a única sobrevivente, sua irmã Nezuko Kamado, transformada em um Demônio. Para sua surpresa, no entanto, Nezuko ainda mostra sinais de emoção e pensamento humanos. Assim começa a jornada de Tanjiro para procurar o Demônio que matou sua família e transformar sua irmã humana novamente. 	0	24m	c30b4ffab01dedf23befc2486be5c829	19695f8da0ee2a83b29376dfafc83e0b	8.0	\N	\N
83	Plastic Memories 	2015-04-04	Esta história se passa em um futuro não muito distante, quando androides que parecem exatamente como os seres humanos começam a se espalhar pelo mundo. A empresa de produção de androides SA Corp. produziu Giftia, um novo tipo de androide que tem a maior quantidade de emoção e qualidades semelhantes aos humanos fora de qualquer outro modelo já visto. No entanto, devido a problemas na tecnologia, os androides possuem uma vida útil e uma vez que eles passam disso, eles… Bem, eles ficam muito ruins. Por esta razão, SA Corp. cria um serviço de terminal, a fim de recuperar os Giftia que passaram do seu tempo de vida útil.\n\n	0	24m	741ddab68c9ae615c92e60447e1ef306	755278032b009759fabab1bc7d053036	7.5	\N	\N
85	Eu Posso Ouvir o Oceano	1993-05-05	Após o divórcio dos pais de Rikako Muto, uma estudante de Tóquio, ela é transferida para um colégio de Kochi, uma cidade litorânea remota da capital. Além de linda e inteligente, se dedica muito aos estudos e aos esportes. Mas sem saber porquê, não consegue se adaptar à vida social da escola. No mesmo colégio chegam Taku Morisaki e Yutaka Matsuno, que sempre foram melhores amigos, e logo o trio começa a viver um triângulo amoroso.	1	1h 12m	bda5ec5e3c69f8663d892d8249faf76d	bd2678fe55034145fb19f72b58f99ffb	8.5	\N	\N
84	Darling in the Franxx 	2015-04-04	A história está definida no futuro distante. A terra está arruinada, e a humanidade estabelece a plantação móvel da fort city. Os pilotos produziram dentro de Plantation ao vivo em Mistilteinn, também conhecido como "birdcage". As crianças vivem ali sem saber nada do mundo exterior nem da liberdade do céu. Suas vidas consistem em lutar para realizar missões. Seus inimigos são misteriosas formas de vida gigantes conhecidas como Kyōryū, e os robôs pilotos das crianças chamaram Franxx para enfrentarem-se contra eles. 	0	24m	1fd42c8f94dffd499019edacb82d30d4	41e0a67d019e50348e020fb8942610f1	8.0	\N	\N
86	Memórias de Ontem	1991-07-20	Conta a história de Taeko Okajima, uma mulher de 27 anos e ainda solteira, com um trabalho burocrático num escritório em Tokyo, cidade na qual nasceu e cresceu. Quando criança, Taeko morria de inveja das amigas que podiam ir para o campo e visitar os parentes no período de férias. Por não possuir parentes no interior, Taeko tinha de permanecer em Tokyo o tempo todo, sonhando com o dia em que, finalmente, poderia realizar seu desejo de conhecer o campo.	1	1h 12m	69359871c3c77935e6e5b123f29370bf	2d0c8d377c6610093065f82b946ac89f	8.5	\N	\N
87	As Memórias de Marnie	2014-11-19	Anna é uma menina de 12 anos, filha de pais adotivos, sempre muito solitária e não exatamente feliz. Um belo dia, em um castelo numa ilha isolada, ela conhece Marnie. A menina loira de vestido branco se torna a grande e única amiga de Anna, mas ela descobrirá que Marnie não é exatamente quem parece ser.	1	1h 44m	9fb8a14367b6bf6116ef21e44dbd64a4	45d8f6357eb4f4e913aadf2a97c18e25	6.5	\N	\N
\.


--
-- Data for Name: review; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.review (id_review, id_program, ds_title, ds_author, dt_date, ds_text, nr_rating) FROM stdin;
27	34	sdasdasd	Luiz	2022-08-14	asda	4.0
28	34	sdasdasd	Luiz	2022-08-14	asda	3.0
1	3	Evangelion	Luiz Henrique	2003-09-16	Ola mundo	0.0
2	4	nada	nada	2003-12-12	nada	0.0
3	4	nada	nada	2003-12-12	nada	0.0
4	3	nada	nada	2003-12-12	nada	0.0
5	3	ola mUNDO	Luiz	2022-01-09	ADASDASDASD	0.0
6	3	asdasdas	Luiz	2022-01-12	text	0.0
7	3	asdasdas	Luiz	2022-01-12	text	0.0
8	3	Ola mundo, erase e bom	Luiz	2022-01-23	Hoje me depara com algo ao qual nao estou acostumado, A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real	0.0
9	5	asdasd	Luiz	2022-01-23	A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real,A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real	0.0
11	6	Erased, uma resenha por Luiz	Luiz	2022-01-23	E ótimo	0.0
12	34	Resenha	Luiz	2022-01-27	test	0.0
13	33	sdfsdfsdfs	Luiz	2022-01-29	sdfsdfsdfsdf	0.0
14	33	fsdfsdf	Luiz	2022-01-29	dfs	0.0
15	5	Minha rola 	Luiz	2022-04-21	asdasdasdasd	0.0
25	51		Luiz	2022-08-14		0.0
10	5	2312312	Luiz	2022-01-23	A expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo realA expressão Lorem ipsum em design gráfico e editoração é um texto padrão em latim utilizado na produção gráfica para preencher os espaços de texto em publicações para testar e ajustar aspectos visuais antes de utilizar conteúdo real	0.0
16	53	asdagsfd	Luiz	2022-04-21	E muito bom!	0.0
17	32	Resenha	Luiz	2022-05-24	Resenha	0.0
18	34	Minha resenha 	Luiz	2022-08-07	Muito bom 	0.0
19	68	Minha resenha 	Luiz	2022-08-08	Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\n\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.	0.0
20	68	sdasdadasd	Luiz	2022-08-08	Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC\n"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"\n\n1914 translation by H. Rackham\n"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"	0.0
21	35	Ola Mundo	Luiz	2022-08-14	review	0.0
22	35	Ola Mundo	Luiz	2022-08-14	review	0.0
23	35	dasdasd	Luiz	2022-08-14	asdas	0.0
24	35	dasdasd	Luiz	2022-08-14	asdas	0.0
26	51		Luiz	2022-08-14		0.0
29	34		Luiz	2022-08-14		4.0
30	34		Luiz	2022-08-14		4.0
31	73	Castelvania	Luiz	2022-09-04	Otima serie 	4.0
32	44	test	Luiz	2022-09-04	test	4.0
33	34	Tets	Luiz	2022-11-02	asdasdsd	5.0
34	32	asdasdasd	Luiz	2022-12-03	asdasd	3.0
\.


--
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.test (name) FROM stdin;
\.


--
-- Name: genre_id_genre_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.genre_id_genre_seq', 197, true);


--
-- Name: program_id_program_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.program_id_program_seq', 87, true);


--
-- Name: review_id_review_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.review_id_review_seq', 34, true);


--
-- Name: genre genre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genre
    ADD CONSTRAINT genre_pkey PRIMARY KEY (id_sequential);


--
-- Name: program program_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.program
    ADD CONSTRAINT program_pkey PRIMARY KEY (id_program);


--
-- Name: review review_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.review
    ADD CONSTRAINT review_pkey PRIMARY KEY (id_review);


--
-- Name: test test_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.test
    ADD CONSTRAINT test_name_key UNIQUE (name);


--
-- PostgreSQL database dump complete
--

